from busio import BusConnector
from display.ssd_1306 import SSD1306


class SSD1306BusConnector(SSD1306):
    def __init__(self, width, height, external_vcc, bus_connector: BusConnector):
        self._bus_connector = bus_connector
        super().__init__(width, height, external_vcc)

    def __enter__(self):
        self.reset()
        self.poweron()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.reset()
        self.poweroff()

    def write_cmd(self, param):
        self._bus_connector.write([0x80, param])

    def write_data(self, buffer):
        buffer = bytearray([0x40]) + buffer
        self._bus_connector.write(buffer)
