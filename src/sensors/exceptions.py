class SensorError(Exception):
    """Base class for sensor errors."""

    def __init__(self, sensor_id: str, message: str):
        self.sensor_id = sensor_id
        self.message = message
        super().__init__(f'sensor: {sensor_id} ' + self.message)
