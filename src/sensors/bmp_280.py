# -*- coding: utf-8 -*-
"""BMP280 sensor driver for FTDI 232H I2C adapter."""

import logging
from dataclasses import dataclass
from enum import IntEnum
from timing import await_millis

from busio.connector import BusConnector
from .exceptions import SensorError

_logger = logging.getLogger(__name__)


class Reg(IntEnum):
    """Registers of the BMP280."""

    ID = 0xD0
    VALID_HW_ID = 0x58  # Contents of the ID register for a BMP280.
    STATUS = 0xF3
    CONTROL = 0xF4
    CONFIG = 0xF5
    TEMP_MSB = 0xFA
    PRESS_MSB = 0xF7
    # Compensation coefficient registers.
    T1 = 0x88
    T2 = 0x8A
    T3 = 0x8C
    P1 = 0x8E
    P2 = 0x90
    P3 = 0x92
    P4 = 0x94
    P5 = 0x96
    P6 = 0x98
    P7 = 0x9A
    P8 = 0x9C
    P9 = 0x9E


@dataclass(frozen=True, slots=True, repr=True)
class BMP280Data:
    temperature_c: float  # Temperature in degrees Celsius.
    pressure_pa: float  # Pressure in Pascals.


class Bmp280Sensor:
    """BMP280 sensor driver for FTDI 232H
    """

    def __init__(self, sensor_id: str, bus: BusConnector) -> None:
        self._sensor_id = sensor_id
        self._bus = bus
        self._initialized = False
        self._dig_T1 = None
        self._dig_T2 = None
        self._dig_T3 = None
        self._dig_P1 = None
        self._dig_P2 = None
        self._dig_P3 = None
        self._dig_P4 = None
        self._dig_P5 = None
        self._dig_P6 = None
        self._dig_P7 = None
        self._dig_P8 = None
        self._dig_P9 = None

    @property
    def sensor_id(self):
        return self._sensor_id

    @property
    def initialized(self):
        return self._initialized

    def __enter__(self):
        _logger.debug(f'with sensor [{self.sensor_id}]')

        _logger.debug(f'    [{self.sensor_id}] verifying hardware id')
        hw_id = self._hardware_id()
        if hw_id != Reg.VALID_HW_ID:
            raise SensorError(self.sensor_id,
                              f'Invalid hardware id: 0x{hw_id:02x}. Expected: 0x{Reg.VALID_HW_ID:02x} but got 0x{hw_id:02x}')

        _logger.debug(f'    [{self.sensor_id}] reading compensation coefficients')
        bus_connector = self._bus
        self._dig_T1 = float(bus_connector.readU16(Reg.T1))
        self._dig_T2 = float(bus_connector.readS16(Reg.T2))
        self._dig_T3 = float(bus_connector.readS16(Reg.T3))
        self._dig_P1 = float(bus_connector.readU16(Reg.P1))
        self._dig_P2 = float(bus_connector.readS16(Reg.P2))
        self._dig_P3 = float(bus_connector.readS16(Reg.P3))
        self._dig_P4 = float(bus_connector.readS16(Reg.P4))
        self._dig_P5 = float(bus_connector.readS16(Reg.P5))
        self._dig_P6 = float(bus_connector.readS16(Reg.P6))
        self._dig_P7 = float(bus_connector.readS16(Reg.P7))
        self._dig_P8 = float(bus_connector.readS16(Reg.P8))
        self._dig_P9 = float(bus_connector.readS16(Reg.P9))
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        _logger.debug(f'    [{self.sensor_id}] exiting context')
        self._initialized = False

    def __del__(self):
        if self.initialized:
            self.__exit__(None, None, None)

    def _hardware_id(self):
        return self._bus.readU8(Reg.ID)

    def _forcedmode(self):
        """Set the sensor to forced mode."""
        self._bus.write_to(Reg.CONTROL, b"\xfe")

    def read(self) -> BMP280Data:
        """Read the sensor data from the chip and return (temperature, pressure)."""
        # Do one measurement in high resolution, forced mode.
        self._forcedmode()
        bus_connector = self._bus
        # Wait while measurement is running
        while bus_connector.readU8(Reg.STATUS) & 0x08:
            await_millis(10)
        # Now read and process the data.
        UT = bus_connector.readU24(Reg.TEMP_MSB)
        # print("DEBUG: UT = ", UT)
        var1 = (UT / 16384.0 - self._dig_T1 / 1024.0) * self._dig_T2
        # print("DEBUG: var1 = ", var1)
        var2 = (
                       (UT / 131072.0 - self._dig_T1 / 8192.0)
                       * (UT / 131072.0 - self._dig_T1 / 8192.0)
               ) * self._dig_T3
        # print("DEBUG: var2 = ", var2)
        t_fine = int(var1 + var2)
        # print("DEBUG: t_fine = ", t_fine)
        me_temp = t_fine / 5120.0
        # print("DEBUG: self._temp = ", self._temp)
        # Read pressure.
        UP = bus_connector.readU24(Reg.PRESS_MSB)
        # print("DEBUG: UP = ", UP)
        var1 = t_fine / 2.0 - 64000.0
        # print("DEBUG: var1 = ", var1)
        var2 = var1 * var1 * self._dig_P6 / 32768.0
        # print("DEBUG: var2 = ", var2)
        var2 = var2 + var1 * self._dig_P5 * 2.0
        # print("DEBUG: var2 = ", var2)
        var2 = var2 / 4.0 + self._dig_P4 * 65536.0
        # print("DEBUG: var2 = ", var2)
        var1 = (self._dig_P3 * var1 * var1 / 534288.0 + self._dig_P2 * var1) / 534288.0
        # print("DEBUG: var1 = ", var1)
        var1 = (1.0 + var1 / 32768.0) * self._dig_P1
        # print("DEBUG: var1 = ", var1)
        if var1 == 0.0:
            return BMP280Data(me_temp, 0)
        p = 1048576.0 - UP
        # print("DEBUG: p = ", p)
        p = ((p - var2 / 4096.0) * 6250) / var1
        # print("DEBUG: p = ", p)
        var1 = self._dig_P9 * p * p / 2147483648.0
        # print("DEBUG: var1 = ", var1)
        var2 = p * self._dig_P8 / 32768.0
        # print("DEBUG: var2 = ", var2)
        p = p + (var1 + var2 + self._dig_P7) / 16.0
        me_press = p
        # print("DEBUG: self._press = ", self._press)
        return BMP280Data(me_temp, me_press)
