# -*- coding: utf-8 -*-

"""CCS811 gas sensor driver for FTDI 232H I2C adapter.

    breakout board description: https://learn.sparkfun.com/tutorials/ccs811-air-quality-breakout-hookup-guide
    manufacturer datasheets: https://www.sciosense.com/products/end-of-life/ccs811/
"""
import functools
import logging
from dataclasses import dataclass
from decimal import Decimal

from busio import BusConnector
from timing import await_millis
from .exceptions import SensorError

_logger = logging.getLogger(__name__)


@dataclass(frozen=True, slots=True, repr=True)
class CurrentRawVoltage:
    current: int
    voltage: Decimal

    def __str__(self) -> str:
        return f'{self.current} µA @ {self.voltage} V'


@dataclass(frozen=True, slots=True, repr=True)
class GasSensorData:
    sensor_id: str
    co2: int
    tvoc: int

    def __str__(self) -> str:
        return f'sensor {self.sensor_id} eCO2: {self.co2} ppm TVOC: {self.tvoc} ppb'

    @staticmethod
    def from_bytes(sensor_id: str, data: bytes) -> 'GasSensorData':
        if len(data) < 4:
            raise ValueError(f'Expected at least 4 bytes, got {len(data)}')

        co2 = int.from_bytes(data[0:2], byteorder='big')
        tvoc = int.from_bytes(data[2:4], byteorder='big')

        return GasSensorData(sensor_id, co2, tvoc)


@dataclass(frozen=True, repr=True, slots=True)
class GasSensorVersion:
    major: int = 0
    minor: int = 0
    trivial: int = 0

    def __str__(self) -> str:
        return f'{self.major}.{self.minor}.{self.trivial}'


class GasSensor:
    """CCS811 gas sensor driver for FTDI 232H I2C adapter.
       Initialize I2C bus with 100KHz - 400KHz frequency gives false readings.
       Attach WAKE pin to GND to keep sensor in active mode.
       Use sensor as a context manager.

         Example:
            with GasSensor('sensor_id', port) as sensor:
                while not sensor.is_data_ready():
                    pass
                print(sensor.read())
    """
    # @formatter:off
    MODE_IDLE           = 0b00000000    # Idle (Measurements are disabled in this mode)
    MODE_1S             = 0b00010000    # Constant power mode, IAQ measurement every second
    MODE_10S            = 0b00100000    # Pulse heating mode IAQ measurement every 10 seconds
    MODE_60S            = 0b00110000    # Low power pulse heating mode IAQ measurement every 60 seconds
    MODE_250MS          = 0b01000000    # Constant power mode, sensor measurement every 250ms
    MODE_INT_DATARDY    = 0b00001000    # The nINT signal is asserted (driven low) when a new sample is ready in
                                        #    ALG_RESULT_DATA. The nINT signal will stop being driven low when
                                        #    ALG_RESULT_DATA is read on the I²C interface.

    STATUS_REGISTER             = 0X00
    MEASURE_MODE_REGISTER       = 0X01
    ALG_RESULT_DATA_REGISTER    = 0X02
    RAW_DATA_REGISTER           = 0X03
    ENV_DATA_REGISTER           = 0X05
    BASELINE_REGISTER           = 0X11
    HW_ID_REGISTER              = 0X20
    FW_APP_VERSION_REGISTER     = 0x24
    ERROR_ID_REGISTER           = 0XE0

    APP_START_CMD               = [0xF4]
    SW_RESET_CMD                = [0xFF, 0x11, 0xE5, 0x72, 0x8A]

    VALID_HW_ID                 = 0x81

    # @formatter:on

    def __init__(self, sensor_id: str, bus: BusConnector, *, default_mode: int = MODE_1S | MODE_INT_DATARDY):
        self._sensor_id = sensor_id
        self._bus = bus
        self._initialized = False
        self._measure_register_value = default_mode

    def __enter__(self):
        _logger.debug(f'with sensor [{self.sensor_id}]')
        self._sw_reset()

        if not self._is_app_started():
            fw_version = self.app_version
            _logger.debug(f'    [{self.sensor_id}] app version [{fw_version}]')
            _logger.debug(f'    [{self.sensor_id}] booting CCS811 app')
            self._boot_app()

        _logger.debug(f'    [{self.sensor_id}] verifying hardware id')
        hw_id = self._hardware_id()
        if hw_id != self.VALID_HW_ID:
            raise SensorError(self.sensor_id, f'Invalid hardware id: 0x{hw_id:02x}. Expected: 0x{self.VALID_HW_ID:02x}')

        self._initialized = True

        _logger.debug(f'    [{self.sensor_id}] initial MEAS_MODE register value: {self.mode:08b}')
        self.mode = self._measure_register_value

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        _logger.debug(f'    [{self.sensor_id}] exiting context')
        self._sw_reset()
        self._initialized = False

    def __del__(self):
        if self.initialized:
            self.__exit__(None, None, None)

    def _sw_reset(self) -> None:
        _logger.debug(f'    [{self.sensor_id}] resetting')
        self._bus.write(self.SW_RESET_CMD)
        await_millis(100)

    def _boot_app(self) -> None:
        self._bus.write(self.APP_START_CMD)
        await_millis(100)

    def _hardware_id(self) -> int:
        return self._bus.readU8(self.HW_ID_REGISTER)

    def _is_app_started(self) -> bool:
        return self.status() & 0b10000000 != 0

    @staticmethod
    def _verify_initialized(func):
        @functools.wraps(func)
        def inner(self, *args, **kwargs):
            if not self.initialized:
                raise SensorError(self.sensor_id, 'Sensor not initialized')
            return func(self, *args, **kwargs)

        return inner

    @property
    def initialized(self) -> bool:
        return self._initialized

    def hardware_error_id(self) -> int:
        return self._bus.readU8(self.ERROR_ID_REGISTER)

    def status(self) -> int:
        return self._bus.readU8(self.STATUS_REGISTER)

    @property
    @_verify_initialized
    def mode(self) -> int:
        return self._bus.readU8(self.MEASURE_MODE_REGISTER)

    @mode.setter
    @_verify_initialized
    def mode(self, value) -> None:
        self._bus.write_to(self.MEASURE_MODE_REGISTER, [value])
        self._measure_register_value = self.mode
        _logger.debug(f'    [{self.sensor_id}] MEAS_MODE register set to value: {self._measure_register_value:08b}')
        await_millis(100)

    def is_data_ready(self) -> bool:
        status = self.status()
        return _status_is_data_ready(status)

    @_verify_initialized
    def read(self, *, wait_for_data: bool = False, delay: int = 100) -> GasSensorData:
        """
        read sensor data

        :param wait_for_data: if True do while loop with sleep waiting for data to be ready.
                              If False then make sure data is ready by checking interrupt pin (if configured)
                              or checking
        :param delay: sleep time in millis between checking if data is ready.
        :return: a structure containing co2 ppm and tvoc ppb values

        .. seealso:: :func:`~GasSensor.is_data_ready` use this method when not using any hardware interrupt setup
            or interrupts are disabled
        """

        if wait_for_data:
            while not self.is_data_ready():
                await_millis(delay)

        buf = self._bus.read_from(self.ALG_RESULT_DATA_REGISTER, 4)
        return GasSensorData.from_bytes(self.sensor_id, buf)

    @_verify_initialized
    def current_raw_voltage(self) -> CurrentRawVoltage:
        buf = self._bus.read_from(self.RAW_DATA_REGISTER, 2)
        return _convert_raw_data(buf)

    @property
    @_verify_initialized
    def base_line(self) -> bytes:
        return self._bus.read_from(self.BASELINE_REGISTER, 2)

    @base_line.setter
    @_verify_initialized
    def base_line(self, value: bytes) -> None:
        self._bus.write_to(self.BASELINE_REGISTER, value)

    @property
    def sensor_id(self) -> str:
        return self._sensor_id

    @property
    def app_version(self) -> GasSensorVersion:
        buf = self._bus.read_from(self.FW_APP_VERSION_REGISTER, 2)
        major = (buf[0] & 0b11110000) >> 4
        minor = buf[0] & 0b00001111
        trivial = buf[1]
        return GasSensorVersion(major, minor, trivial)


def _status_is_data_ready(status) -> bool:
    return status & 0b00001000 != 0


def _status_has_error(status) -> bool:
    return status & 0b00000001 != 0


def _convert_raw_data(buf: bytes) -> CurrentRawVoltage:
    current = (buf[0] & 0b11111100) >> 2
    v = (((buf[0] & 0b00000011) << 8) | buf[1])
    voltage = Decimal(v * 1.65 / 1023)
    return CurrentRawVoltage(current, round(voltage, 2))
