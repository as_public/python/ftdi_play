from typing import Protocol


class Average(Protocol):
    def update(self, value: float) -> None:
        ...

    def get(self) -> float:
        ...

    def reset(self, *args, **kwargs) -> None:
        """reset current state of average

        :param args: defined by concrete class
        :param kwargs: defined by concrete class
        :return: None
        """
        ...

    def update_count(self) -> int:
        """return number of updates since last reset

        :return: number of updates
        """
        ...


from .moving_average import ArithmeticMovingWindowedAverage
