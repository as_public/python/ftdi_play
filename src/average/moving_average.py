from average import Average


class ArithmeticMovingWindowedAverage(Average):
    """
    Arithmetic Moving Average
    """

    def __init__(self, window_size):
        self.window_size = window_size
        self.values = []
        self.update_counts = 0

    def update(self, value: float):
        """
        Update the moving average with a new value
        :param value: a value
        :return: moving average
        """
        self.values.append(value)
        if len(self.values) > self.window_size:
            self.values = self.values[-self.window_size:]
        self.update_counts += 1

    def get(self) -> float:
        return sum(self.values) / len(self.values)

    def reset(self):
        self.values = []
        self.update_counts = 0

    def update_count(self) -> int:
        return self.update_counts


