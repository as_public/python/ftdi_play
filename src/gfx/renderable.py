from typing import Protocol, Any


class Renderable(Protocol):

    def render(self) -> Any:
        pass
