from dataclasses import dataclass

from framebuf import FrameBuffer
from gfx.renderable import Renderable


@dataclass(frozen=True)
class EnvironmentData:
    co2: int | None  # carbon dioxide (ppm)
    tco: int | None  # total volatile organic compounds (ppb)
    avg_co2: float | None  # average carbon dioxide (ppm)
    temp_c: float | None  # temperature celsius
    pressure_pas: float | None  # pressure pascals


class EnvironmentScreen(Renderable):

    def __init__(self, frameBuffer: FrameBuffer) -> None:
        self._frameBuffer = frameBuffer
        self._data = EnvironmentData(None, None, None, None, None)

    def update(self, data: EnvironmentData) -> 'EnvironmentScreen':
        self._data = data
        return self

    def render(self) -> 'EnvironmentScreen':
        frame = self._frameBuffer
        data = self._data

        frame.fill(0)
        frame.rect(0, 0, 128, 32, 255)
        start_y = 2
        start_x = 2
        frame.text(f'co2  : {data.co2} tco: {data.tco}', start_x, start_y, 255)
        start_y += 10
        frame.text(f'temp : {EnvironmentScreen._round_or_none(data.temp_c)} C', start_x, start_y, 255)
        start_y += 10
        frame.text(f'press: {EnvironmentScreen._round_or_none(data.pressure_pas, divisor=100)} hPa', start_x, start_y,
                   255)
        start_y += 10

        return self

    @staticmethod
    def _round_or_none(value: float, *, divisor: float = 1.0, scale: int = 1) -> float:
        return round(value / divisor, scale) if value is not None else None
