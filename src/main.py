import logging
import os
import time
from enum import IntEnum

from pyftdi.ftdi import Ftdi
from pyftdi.gpio import GpioAsyncController, GpioMpsseController, GpioPort
from pyftdi.i2c import I2cController, I2cNackError, I2cPort

import timing
from average import Average, ArithmeticMovingWindowedAverage
from busio.i2c_ftdi import FtdiI2cConnector
from display.ssd1306_busconnector import SSD1306BusConnector
from sensors import GasSensor, Bmp280Sensor
from ui.env_screen import EnvironmentScreen, EnvironmentData

"""
https://eblot.github.io/pyftdi/
"""

__logger = logging.getLogger(__name__)

FTDI_URL = 'ftdi:///1'


class I2cDevices(IntEnum):
    CCS_811 = 0x5a
    BMP_280 = 0x76
    SSD_1306 = 0x3c


def main():
    gpio = GpioMpsseController()
    i2c = I2cController()
    try:
        gpio.configure(FTDI_URL, direction=0x0100 ^ 0xFFFF, frequency=10.0)
        i2c.configure(FTDI_URL, frequency=I2cController.DEFAULT_BUS_FREQUENCY)
        sensor_io(i2c.get_port(I2cDevices.CCS_811),
                  i2c.get_port(I2cDevices.BMP_280),
                  i2c.get_port(I2cDevices.SSD_1306),
                  gpio.get_gpio())
    finally:
        i2c.close()


def sensor_io(gas_sensor_port: I2cPort, bmp_sensor_port: I2cPort, ssd1306_port: I2cPort, gpio: GpioPort):
    window_minutes = 10
    count = 0

    with GasSensor("ccs811", FtdiI2cConnector(gas_sensor_port)) as gas, \
            Bmp280Sensor("bmp280", FtdiI2cConnector(bmp_sensor_port)) as bmp, \
            SSD1306BusConnector(128, 32, False, FtdiI2cConnector(ssd1306_port)) as oled:

        gas_sensor_base_line = load_ccs811_base_line(gas.sensor_id)
        if gas_sensor_base_line is not None:
            gas.base_line = gas_sensor_base_line

        co2_avg: Average = ArithmeticMovingWindowedAverage(window_minutes * 60)
        screen = EnvironmentScreen(oled)
        while True:
            try:
                count += 1
                while not ((gpio.read()[0] & 0x0100) >> 8) == 0:
                    timing.await_millis(100)
                gas_sensor_data = gas.read()
                bmp_sensor_data = bmp.read()
                co2_avg.update(gas_sensor_data.co2)

                __logger.debug(
                    f'current co2: {gas_sensor_data.co2} ppm, average co2/{window_minutes} min: {round(co2_avg.get(), 1)} ppm, current tvoc:'
                    f' {gas_sensor_data.tvoc} '
                    f'ppb, '
                    f'temperature: {round(bmp_sensor_data.temperature_c, 1)} ℃, pressure: {round(bmp_sensor_data.pressure_pa / 100, 1)} hPa')

                screen.update(EnvironmentData(gas_sensor_data.co2,
                                              gas_sensor_data.tvoc,
                                              co2_avg.get(),
                                              bmp_sensor_data.temperature_c,
                                              bmp_sensor_data.pressure_pa))
                screen.render()
                oled.show()
            except KeyboardInterrupt:
                __logger.debug(f'KeyboardInterrupt, {count} reads')
                store_ccs811_base_line(gas.sensor_id, gas.base_line)
                break


def ftdi_io():
    d = Ftdi()
    try:
        d.open_from_url(FTDI_URL)
        print(f'device port count {d.device_port_count}')
        print(f'device port width {d.port_width}')
        print(f'ic name {d.ic_name}')
        print(f'device baudrate {d.baudrate}')
        print(f'device version {d.device_version}')
        print(f'device usb_path {d.usb_path}')
        print(f'device is_connected {d.is_connected}')
        print(f'device has_cbus {d.has_cbus}')
        print(f'device has_mpsse {d.has_mpsse}')
        print(f'device has_drivezero {d.has_drivezero}')
        print(f'device has_wide_port {d.has_wide_port}')
        print(f'device is_H_series {d.is_H_series}')
    finally:
        d.close()


def async_io():
    gpio = GpioAsyncController()
    try:
        gpio.configure(FTDI_URL, direction=0xFF, frequency=1E3)
        print(f'async all pins {gpio.all_pins}')
        print(f'async pins {gpio.pins}')
        print(f'async width {gpio.width}')
        gpio.write(0x00)
    finally:
        gpio.close()


def mpsse_io():
    mpsse = GpioMpsseController()
    try:
        mpsse.configure(FTDI_URL, direction=0b1111111111111111, frequency=10)
        print(f'mpsse all pins {mpsse.all_pins}')
        print(f'mpsse pins {mpsse.pins}')
        print(f'mpsse width {mpsse.width}')
        print(f'mpsse direction {mpsse.direction:016b}')
        count = 0
        while count < 2:
            mpsse.write(0b1111111111111111)
            time.sleep(1)
            mpsse.write(0b1101111111111111)
            time.sleep(1)
            count += 1
    except KeyboardInterrupt:
        pass
    finally:
        mpsse.close()


def i2c_io():
    i2c = I2cController()
    try:
        i2c.configure(FTDI_URL, frequency=I2cController.HIGH_BUS_FREQUENCY)
        print(f'i2c frequency {i2c.frequency}')
        print(f'i2c frequency_max {i2c.frequency_max}')
        print(f'i2c pins {i2c.gpio_pins}')
        print(f'i2c all pins {i2c.gpio_all_pins}')
        print(f'i2c width {i2c.width}')
        print(f'i2c direction {i2c.direction:016b}')
        addresses = [0x5a, 0x5b]
        for addr in addresses:
            port = i2c.get_port(addr)
            try:
                print(f'i2c get_port {addr:00x} read {port.read(0, True, True)}')
            except I2cNackError as e:
                print(f'i2c get_port {addr:00x} {e})')

    finally:
        i2c.close()


def store_ccs811_base_line(sensor_id: str, base_line: bytes):
    file_name = get_file_name(sensor_id)
    with open(file_name, 'wb') as f:
        buf_str = ' '.join([f'{i}: 0x{b:02x}' for i, b in enumerate(base_line)])
        __logger.debug(f'storing {len(base_line)} bytes of base_line: {buf_str} in {file_name}')
        f.write(base_line)


def load_ccs811_base_line(sensor_id: str) -> bytes | None:
    file_name = get_file_name(sensor_id)
    if os.path.exists(file_name):
        with open(file_name, 'rb') as f:
            buf = f.read(2)
            buf_str = ' '.join([f'{i}: 0x{b:02x}' for i, b in enumerate(buf)])
            __logger.debug(f'loaded {len(buf)} bytes of base_line: {buf_str}')
            return buf
    else:
        __logger.debug(f'no base_line file {file_name}')
    return None


def get_file_name(sensor_id: str) -> str:
    path = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', f'{sensor_id}_base_line.bin'))
    return path


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
    try:
        timing.replace_implementation(lambda x: time.sleep(1.0 * x / 1000.0))
        main()
    except Exception as exc:
        __logger.exception(exc, exc_info=True)
