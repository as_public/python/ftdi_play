import time
import asyncio


def __await_millis_with_exception__(millis: int) -> None:
    """
    It is just a prototype if a method that should be patched in runtime to meet application requirements
    """
    raise NotImplementedError(f"Please substitute {await_millis.__name__} with sth real.")


__await_millis_implementation__ = __await_millis_with_exception__


def replace_implementation(new_wait_millis_implementation) -> None:
    global __await_millis_implementation__
    __await_millis_implementation__ = new_wait_millis_implementation


def await_millis(millis: int) -> None:
    __await_millis_implementation__(millis)
