from typing import Protocol, Union, Iterable


class BusConnector(Protocol):
    """Protocol for bus connector."""

    def readU8(self, register: int) -> int:
        """Read an unsigned byte from the specified register"""
        ...

    def readU16(self, register: int) -> int:
        """Read an unsigned short from the specified register"""
        ...

    def readS16(self, register: int) -> int:
        """Read an unsigned short from the specified register"""
        ...

    def readU24(self, register: int) -> int:
        """Read 3 bytes from bus and convert to int."""
        ...

    def read(self, readlen: int = 0) -> bytes:
        """Read one or more bytes from a remote slave

          :param readlen: count of bytes to read out.
          :param relax: whether to relax the bus (emit STOP) or not
          :param start: whether to emit a start sequence (w/ address)
          :return: byte sequence of read out bytes
        """
        ...

    def read_from(self, register: int, readlen: int = 0,
                  relax: bool = True, start: bool = True) -> bytes:
        """Read one or more bytes from a remote slave

           :param register: slave register address to read from
           :param readlen: count of bytes to read out.
           :param relax: whether to relax the bus (emit STOP) or not
           :param start: whether to emit a start sequence (w/ address)
           :return: data read out from the slave
        """
        ...

    def write_to(self, register: int,
                 out: Union[bytes, bytearray, Iterable[int]],
                 relax: bool = True, start: bool = True) -> None:
        """Write one or more bytes to a remote slave

           :param register: slave register address to write to
           :param out: the byte buffer to send
           :param relax: whether to relax the bus (emit STOP) or not
           :param start: whether to emit a start sequence (w/ address)
        """
        ...

    def write(self, out: Union[bytes, bytearray, Iterable[int]],
              relax: bool = True, start: bool = True) -> None:
        """Read one or more bytes from a remote slave

           :param out: the byte buffer to send
           :param relax: whether to relax the bus (emit STOP) or not
           :param start: whether to emit a start sequence (w/ address)
        """
        ...

    def exchange(self, out: Union[bytes, bytearray, Iterable[int]] = b'',
                 readlen: int = 0,
                 relax: bool = True, start: bool = True) -> bytes:
        """Perform an exchange or a transaction with the I2c slave

           :param out: an array of bytes to send to the I2c slave,
                       may be empty to only read out data from the slave
           :param readlen: count of bytes to read out from the slave,
                       may be zero to only write to the slave
           :param relax: whether to relax the bus (emit STOP) or not
           :param start: whether to emit a start sequence (w/ address)
           :return: data read out from the slave
        """
        ...
