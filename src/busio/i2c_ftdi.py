from typing import Union, Iterable

from pyftdi.i2c import I2cPort

from busio.connector import BusConnector


class FtdiI2cConnector(BusConnector):

    def __init__(self, i2c_port: I2cPort) -> None:
        super().__init__()
        self._i2c_port = i2c_port

    def readU8(self, register: int) -> int:
        return self.read_from(register, 1)[0]

    def readU16(self, register: int) -> int:
        data = self.read_from(register, 2)
        return data[1] << 8 | data[0]

    def readS16(self, register: int) -> int:
        result = self.readU16(register)
        if result > 32767:
            result -= 65536
        return result

    def readU24(self, register: int) -> int:
        data = self.read_from(register, 3)
        rv = int((data[0] << 16 | data[1] << 8 | data[2]) >> 4)
        return rv

    def read(self, readlen: int = 0):
        return self._i2c_port.read(readlen)

    def read_from(self, register: int, readlen: int = 0, relax: bool = True, start: bool = True) -> bytes:
        return self._i2c_port.read_from(register, readlen, relax, start)

    def write(self, out: Union[bytes, bytearray, Iterable[int]], relax: bool = True, start: bool = True) -> None:
        self._i2c_port.write(out, relax, start)

    def write_to(self, register: int, out: Union[bytes, bytearray, Iterable[int]], relax: bool = True, start: bool = True) -> None:
        self._i2c_port.write_to(register, out, relax, start)

    def exchange(self, out: Union[bytes, bytearray, Iterable[int]] = b'', readlen: int = 0, relax: bool = True, start: bool = True) -> bytes:
        return self._i2c_port.exchange(out, readlen, relax, start)
